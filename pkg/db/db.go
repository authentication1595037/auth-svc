package db

import (
	"gitlab.com/azonnix/authentication/auth-svc/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Handler struct {
	DB *gorm.DB
}

func Init(url string) (Handler, error) {
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{})
	if err != nil {
		return Handler{}, err
	}

	db.AutoMigrate(&models.User{})

	return Handler{db}, nil
}
